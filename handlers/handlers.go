package handlers

import (
	"api-gateway/kafka"
	"api-gateway/models"
	"context"
	"encoding/json"
	"log"
	"os"

	pb "api-gateway/protos"

	"github.com/IBM/sarama"
	"github.com/gofiber/contrib/websocket"
	"github.com/gofiber/fiber/v2"
	_ "github.com/joho/godotenv/autoload"
)

var (
	sysProducer sarama.AsyncProducer
	highload    kafka.Topic
	post        kafka.Topic
	updates     kafka.Topic
	notices     kafka.Topic
	updatesCh   = make(chan []byte)
	noticesCh   = make(chan []byte)
	MicroClient pb.ExchangeService
)

func Kafka() {
	sysProducer = kafka.NewProd()
	highload.Fill(os.Getenv("HIGHLOAD"))
	post.Fill(os.Getenv("POST"))
	updates.Fill(os.Getenv("UPDATES"))
	notices.Fill(os.Getenv("NOTICES"))
	go notices.Consume(noticesCh)
	go updates.Consume(updatesCh)
}

func WsNotices(c *websocket.Conn) {
	for {
		msg := <-noticesCh
		err := c.WriteMessage(websocket.TextMessage, msg)
		if err != nil {
			log.Printf("Sending message to client failed: %v", err)
			break
		}
	}
}

func WsData(c *websocket.Conn) {
	for {
		request := &pb.StringRequest{
			Message: "/get",
		}
		response, err := MicroClient.Path(context.Background(), request)
		if err != nil {
			log.Printf("Error calling service: %v", err)
		}
		log.Printf("Response: %s\n", string(response.Reply))
		err = c.WriteMessage(websocket.TextMessage, response.Reply)
		if err != nil {
			log.Printf("Sending data to client failed: %v", err)
			break
		}
		<-updatesCh
	}
}

func ReqHighload(c *fiber.Ctx) error {
	resp := highload.Produce([]byte{0}, sysProducer)
	noticesCh <- []byte(resp)
	return nil
}

func GetHighload(c *fiber.Ctx) error {
	var results []models.Calc
	request := &pb.StringRequest{
		Message: "/get-highload",
	}
	response, err := MicroClient.Path(context.Background(), request)
	if err != nil {
		log.Printf("Error calling service: %v", err)
	}
	log.Printf("Response: %s\n", string(response.Reply))
	err = json.Unmarshal([]byte(response.Reply), &results)
	if err != nil {
		log.Printf("JSON deserializing failed: %v", err)
		return err
	}
	return c.JSON(fiber.Map{
		"results": results,
	})
}

func ReqPost(c *fiber.Ctx) error {
	var entry models.Entry
	if err := c.BodyParser(&entry); err != nil {
		log.Printf("Context parsing failed: %v", err)
		return err
	}
	jsonData, err := json.Marshal(entry)
	if err != nil {
		log.Printf("Serializing to JSON failed: %v", err)
		return err
	}
	resp := post.Produce(jsonData, sysProducer)
	noticesCh <- []byte(resp)
	return nil
}

func ReqDelAll(c *fiber.Ctx) error {
	request := &pb.StringRequest{
		Message: "/delete-all",
	}
	response, err := MicroClient.Path(context.Background(), request)
	if err != nil {
		log.Printf("Error calling service: %v", err)
	}
	log.Printf("Response: %s\n", string(response.Reply))
	noticesCh <- []byte(response.Reply)
	return nil
}
