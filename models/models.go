package models

import (
	"time"

	"gorm.io/gorm"
)

type Entry struct {
	gorm.Model
	Content string
}

type Calc struct {
	Date    time.Time
	Message string
}
