package main

import (
	"api-gateway/handlers"
	"api-gateway/kafka"
	"log"
	"os"

	pb "api-gateway/protos"

	"go-micro.dev/v4"

	"github.com/gofiber/contrib/websocket"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	_ "github.com/joho/godotenv/autoload"
)

var (
	sysExch kafka.Topic
	userMsg kafka.Topic
)

func main() {
	// Create Go Micro client
	service := micro.NewService(
		micro.Name("api-gateway"),
	)
	service.Init()
	client := pb.NewExchangeService("data-service", service.Client())
	handlers.MicroClient = client

	// Start Kafka
	sysExch.Fill(os.Getenv("SYS_EXCH"))
	userMsg.Fill(os.Getenv("USER_MSG"))
	topics := kafka.Topics{
		sysExch,
		userMsg,
	}
	kafka.Start(os.Getenv("AK_ADDR"), topics)
	handlers.Kafka()

	// Run Fiber router
	app := app()
	log.Fatal(app.Listen("0.0.0.0:8080"))
}

func app() *fiber.App {
	// Settings
	app := fiber.New()
	app.Use(cors.New())

	// Routes
	app.Get("/messages", websocket.New(handlers.WsNotices))
	app.Get("/get", websocket.New(handlers.WsData))
	app.Post("/post", handlers.ReqPost)
	app.Get("/run-highload", handlers.ReqHighload)
	app.Get("/get-highload", handlers.GetHighload)
	app.Delete("/delete-all", handlers.ReqDelAll)

	return app
}
