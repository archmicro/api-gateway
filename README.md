# API Gateway
The main gateway for processing and routing requests. Processes HTTP
requests and maintains a websocket connection with the client. Sends
asynchronous data generation requests to highload microservice via
Apache Kafka. Connects to data microservice via gRPC to obtain
information from the database. This microservice is under development.